from apps.config.settings import env_template

class Response:
	def __init__(self):
		self.resp_data = {}
		self.status = '200 OK'
		self.headers = []
		self.body = []

	def set_headers(self, param={}):
		# disini akan ada pengaturan headers dan status response
		pass

	def json(self, resp_body):
		self.headers = [
							('Content-Type', 'application/json'),
							('Content-Length', str(len(resp_body)) ),
							('Accept-Encoding', 'gzip, deflate'),
						]

		self.resp_data = {
			'body': resp_body,
			'status': self.status,
			'headers': self.headers
		}

		return self.resp_data

	def render(self, view_path, view_param={}):

		# response data menggunakan Jinja2 template engine
		templates = env_template.get_template(view_path)
		t_render = templates.render().encode("utf-8")
	
		resp_body = t_render
		self.resp_data = self.send(resp_body)

		return self.resp_data
		
	def send(self, resp_body):
		self.headers = [
							('Content-Type', 'text/html'),
							('Content-Length', str(len(resp_body)) ),
							('Accept-Encoding', 'gzip, deflate'),
						 ]

		self.resp_data = {
			'body': resp_body,
			'status': self.status,
			'headers': self.headers
		}

		return self.resp_data

	def remove_headers_item(self, header_item):
		# disini akan ada penghapusan headers
		for item in self.headers:
			print item
			if item[0] == header_item:
				self.headers.remove(item)		
			
				
