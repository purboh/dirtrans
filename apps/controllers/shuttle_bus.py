from apps.models.endpoint import EndpointModel
from cores.response import Response
import json

class Shuttle_bus:
	def __init__(self, request):
		self.request = request

	def list(self):
		message = "Query success.."
		results = EndpointModel().get_list('shuttle_bus')
		
		if (len(results) > 0):
			message = "Query success.."
			status = 200
		else:
			message = "Query failed. Item not found"
			status = 404

		resp_body = json.dumps({"status":status, "message":message, "results":results})
		return Response().json(resp_body)

	def city(self):
		
		results = EndpointModel().by_city(self.request.PARAM[0], 'shuttle_bus')
		request_data = {"get":self.request.GET, "post":self.request.POST, "query_string":self.request.raw_query_string, "request_body":self.request.raw_request_body}
		
		if (len(results) > 0):
			message = "Query success.."
			status = 200
		else:
			message = "Query failed. Item not found"
			status = 404

		resp_body = json.dumps({"status":status, "message":message, "results":results})
		return Response().json(resp_body)
	
	def detail(self):

		results = EndpointModel().detail(self.request.PARAM[0], 'shuttle_bus')
		
		if (len(results) > 0):
			message = "Query success.."
			status = 200
		else:
			message = "Query failed. Shuttle Bus not found"
			status = 404

		resp_body = json.dumps({"status":status, "message":message, "results":results})
		return Response().json(resp_body)
