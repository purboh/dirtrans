from cores.response import Response

class Dashboard:
	"""
	Halaman umum dashboard seperti menu dan grafik
	"""
	def __init__(self, request):
		self.request = request

	def index(self):
		return Response().render('index.html')