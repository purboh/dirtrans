from apps.config.databases import *
import sys
import MySQLdb

class EndpointModel:
	def __init__(self):
		self.db = MySQLdb.connect(**db_config)
		self.cursor = self.db.cursor()

	def get_list(self, endpoint_type):
		sql = "select * from endpoint where type ='"+endpoint_type+"' limit 0, 10 " 
		results = []
		try:
		    self.cursor.execute(sql)
		    temp_results = self.cursor.fetchall()
		    for row in temp_results:
				latitude = "0.0"
				if row[3] != None: latitude = row[3]

				longitude = "0.0"
				if row[4] != None: longitude = row[4]

				endpoint_code = ""
				if row[5] != None: endpoint_code = row[5]

				last_update = row[8].strftime('%Y-%m-%d %H:%M')
				temp_row = {
					"id_endpoint":row[0], 
					"name":row[1], 
					"latitude":latitude,
					"longitude":longitude,
					"endpoint_code":endpoint_code,
					"last_update": last_update
				}

				results.append(temp_row)

		    print "Query berhasil..."
		    return results
		except:
			print "Unexpected error:", sys.exc_info()[0]
			print "Query gagal..."
			return results
		finally:
			self.cursor.close()	
			self.db.close()
		
	def by_city(self, city_id, endpoint_type):
		sql = "select * from endpoint where city_id = "+str(city_id)+" and type ='"+endpoint_type+"'" 
		results = []
		try:
		    self.cursor.execute(sql)
		    temp_results = self.cursor.fetchall()
		    for row in temp_results:
				latitude = "0.0"
				if row[3] != None: latitude = row[3]

				longitude = "0.0"
				if row[4] != None: longitude = row[4]

				endpoint_code = ""
				if row[5] != None: endpoint_code = row[5]

				last_update = row[8].strftime('%Y-%m-%d %H:%M')

				temp_row = {
					"id_endpoint":row[0], 
					"name":row[1], 
					"latitude":latitude,
					"longitude":longitude,
					"endpoint_code":endpoint_code,
					"last_update": last_update
				}

				results.append(temp_row)

		    print "Query berhasil..."
		    return results
		except:
			print "Unexpected error:", sys.exc_info()[0]
			print "Query gagal..."
			return results
		finally:
			self.cursor.close()	
			self.db.close()

	def detail(self, endpoint_id, endpoint_type):
		sql = "select * from endpoint where id_endpoint = "+str(endpoint_id)+" and type ='"+endpoint_type+"'" 
		results = dict()
		try:
			self.cursor.execute(sql)
			row = self.cursor.fetchone()
			
			
			latitude = "0.0"
			if row[3] != None: latitude = row[3]
			
			longitude = "0.0"
			if row[4] != None: longitude = row[4]
			
			last_update = row[8].strftime('%Y-%m-%d %H:%M')
			
			temp_row = {
				"id_endpoint":row[0], 
				"name":row[1], 
				"description": row[2],
				"latitude":latitude,
				"longitude":longitude,
				"endpoint_code":row[5],
				"city_id": row[7],
				"last_update": last_update
			}

			results.update(temp_row)

			print "Query berhasil..."
			return results
		except:
			print "Unexpected error:", sys.exc_info()[0]
			print "Query gagal..."
			return results
		finally:
			self.cursor.close()	
			self.db.close()	
        